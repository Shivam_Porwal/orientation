# GitLab

GitLab is a Git-based repository manager and a powerful complete application for software development.

With an "user-and-newbie-friendly" interface, GitLab allows you to work effectively, both from the command line and from the UI itself. It's not only useful for developers, but can also be integrated across your entire team to bring everyone into a single and unique platform.

The GitLab Workflow logic is intuitive and predictable, making the entire platform easy to use and easier to adopt. Once you do, you won't want anything else!

## Code Review with GitLab

After discussing a new proposal or implementation in the issue tracker, it's time to work on the code. You write your code locally and, once you're done with your first iteration, you commit your code and push to your GitLab repository. Your Git-based management strategy can be improved with the GitLab Flow.

### First Commit
In your first commit message, you can add the number of the issue related to that commit message. By doing so, you create a link between the two stages of the development workflow: the issue itself and the first commit related to that issue.

To do so, if the issue and the code you're committing are both in the same project, you simply add #xxx to the commit message, where xxx is the issue number. If they are not in the same project, you can add the full URL to the issue (https://gitlab.com/<username>/<projectname>/issues/<xxx>).

### Merge Request
Once you push your changes to a feature-branch, GitLab will identify this change and will prompt you to create a Merge Request (MR).

Every MR will have a title (something that summarizes that implementation) and a description supported by Markdown. In the description, you can shortly describe what that MR is doing, mention any related issues and MRs (creating a link between them), and you can also add the issue closing pattern, which will close that issue(s) once the MR is merged.

GitLab Workflow helps your team to get faster from idea to production using a single platform:

 - It's effective, because you get your desired results.
 - It's efficient, because you achieve maximum productivity with minimum effort and expense.
 - It's productive, because you are able to plan effectively and act efficiently.
 - It's easy, because you don't need to set up different tools to accomplish what you need with just one, GitLab.
 - It's fast, because you don't need to jump across multiple platforms to get your job done.

A new GitLab version is released every single month (on the 22nd), for making it a better integrated solution for software development, and for bringing teams to work together in one single and unique interface.

At GitLab, everyone can contribute! 