**What Is Git?**


Git is a free and open source distributed version control system, originally created by Linus Torvalds. Git is distributed i.e every developer has the full history of their code repository locally.
Git is what is known as an open-source version control system which means that it records files over a period of time and these changes can be recalled at a later date. You can do a lot with Git whether it can be branching (creating something that is different from the master branch (the one you would most likely be working on)) or just committing to a repository (programming jargon simply calls it a repo).

**Using GitBash/Terminal to Access GitHub**


Configure Git via git config --global user.name "[name]" and git config --global user.email "[email address]"
Navigate to your working directory (Keep in mind you cannot just cd to the directory, you have to work your way to it, so I personally keep a folder called Programming in my home directory)
Initialize a Git Repo via git init
Now, this is where you can branch-of, you have two options, pushing a new repo or pushing a preexistent repo.
Pushing a New Repo
Commit your repo via git commit -m "first commit"
Remote add your repo via git remote add origin <url>
Push to your repo via git push -u origin master
For Pushing an Existing Repo
Remote add your repo via git remote add origin <url>
Push to your repo via git push -u origin master
Now that you have your repo set up, these are some helpful commands:
git status Used to check what has changed ie additions and deletions
git add <file> Used to add files to commit if used with a period (.) it adds all of the files
git commit -m "message" Use to commit changed, but it is on the local system, the -m can be changed to things such as -u which is an update but it is recommended to keep with an -m
git push Used to push changes to GitHub
git reset Can be used after commit to reset the commits (Good if you accidentally add a file you did not want)
git pull <url> Can be used to pull from any git repo, leave the URL out if your updating your current repo


**.gitignore**

The .gitignore file is useful for stopping certain files from committing automatically. It should automatically be in a repo when you create a project. To use it just cd to the directory where the file you want to exclude is and use pwd to find the directory pathing. Then copy the path into the file, it should look like a text file, and then add the name of the file you want to exclude.



**Branching in Git**


Branching is useful when many people are working on the same project or when you have multiple versions of the same project. The major advantage of branching is when you want to add a feature without compromising the integrity of the master branch.

Branching Commands

git branch [branch-name] Used to create a new branch

git checkout [branch-name] Used to switch branches

git merge [branch] Used to merge branch commits (usually people use this with a branch and the master)

git branch -d [branch-name] Used to delete a branch

